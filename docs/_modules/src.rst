src package
===========

Submodules
----------

src.GPIO\_Fake module
---------------------

.. automodule:: src.GPIO_Fake
    :members:
    :undoc-members:
    :show-inheritance:

src.settings module
-------------------

.. automodule:: src.settings
    :members:
    :undoc-members:
    :show-inheritance:

src.shotmachine module
----------------------

.. automodule:: src.shotmachine
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: src
    :members:
    :undoc-members:
    :show-inheritance:

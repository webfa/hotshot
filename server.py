# -*- coding: cp1252 -*-
from flask import Flask, render_template, request, flash, session, redirect, url_for, jsonify
import sys
import os
import time
from src.settings import Settings
from src.shotmachine import ShotMachine

app = Flask(__name__)

app.config.update(dict(
    DEBUG=False
))    

@app.route('/')
def index():
    global shotMachine
    machineInformation = {"DrinkVariations":shotMachine.getNumberOfDiffShots(),"NumberOfSlots":shotMachine.getNumberOfSlots()}
    return render_template('./html/index.html',schnapsList = shotMachine.getShots(),machineInformation = machineInformation)

@app.route('/settings')
def settings():
    pass

@app.route('/move')
def move():
    pass

@app.route('/order')
def order():
    global shotMachine
    orders = request.args.getlist('orders[]',type=int)
    print orders
    shotMachine.serveDrinks(orders)
    return jsonify(result = 'Done')

@app.route('/game')
def game():
    competitors = request.args.get('competitors',0,type = int)
    print competitors
    return jsonify(result = 'Done')

@app.errorhandler(404)
def page_not_found(e):
    pass

if __name__ =='__main__':
    global shotMachine
    shotMachine = ShotMachine("./src/settings.xml")
    app.run(threaded=True)

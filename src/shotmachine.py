# -*- coding: cp1252 -*-
from flask import Flask, render_template, request, flash, session, redirect, url_for, jsonify
from settings import Settings
import GPIO_Fake as GPIO
import time
import Schrittmotor-Steuerung_V1-0 as stepper

class ShotMachine(object):
    def __init__(self, settingsFile):
        settings = Settings(settingsFile)
        self.__slots = settings.getSlots()
        self.__drinks = settings.getDrinks()
        self.__getSteps = settings.getSteps()
        self.__flowConstant = settings.getFlowConstant()
        self.__users = settings.getUsers()
        self.__quantity = settings.getQuantity()

        for drink in self.__drinks:
            GPIO.setup(drink["pin"],GPIO.OUT)
        
        del settings

    def getShots(self):
        i = 0
        shots = []
        for drink in self.__drinks:
            shot = {"name":drink["name"],"id":"element"+str(i),"number":i}
            shots.append(shot)
            i = i + 1

        return shots

    def getNumberOfDiffShots(self):
        return len(self.__drinks)

    def getNumberOfSlots(self):
        return self.__slots

    def setDrinks(self):
        pass

    def serveDrinks(self,orders):
        for i in range(len(orders)):
            for k in range(orders[i]):
                GPIO.output(self.__drinks[i]["pin"],GPIO.HIGH)
                time.sleep(self.__calculateFlowTime())
                GPIO.output(self.__drinks[i]["pin"],GPIO.LOW)
                stepper.move()

    def __calculateFlowTime(self):
        return float(self.__quantity/self.__flowConstant)

if __name__=='__main__':
    obj = ShotMachine("settings.xml")
    obj.serveDrinks([1,2,3])
            
        

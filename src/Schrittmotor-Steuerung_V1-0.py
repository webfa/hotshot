# Schrittmotor-Steuerung V1.0

import time
from time import sleep
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

# GPIO Zuweisung

A = 6 # IN 1
B = 13 # IN 3
C = 19 # IN 2
D = 26 # IN 4
sensor = 22


# GPIO als Output definieren

GPIO.setup(A, GPIO.OUT)
GPIO.setup(B, GPIO.OUT)
GPIO.setup(C, GPIO.OUT)
GPIO.setup(D, GPIO.OUT)
GPIO.setup(sensor, GPIO.IN)

# Motor im Ruhezustand

GPIO.output(A, False)
GPIO.output(B, False)
GPIO.output(C, False)
GPIO.output(D, False)


# Bipolarer Schrittmotor --> 4 Steps, bei Vollschrittbetieb benötigt
# 4 Steps definieren

delay = 0.05 # Zeit in Sekunden

def Step1():
	GPIO.output(A, True)
	GPIO.output(B, False)
	GPIO.output(C, True)
	GPIO.output(D, False)
	time.sleep(delay)
	
def Step2():
	GPIO.output(A, True)
	GPIO.output(B, False)
	GPIO.output(C, False)
	GPIO.output(D, True)
	time.sleep(delay)
	
def Step3():
	GPIO.output(A, False)
	GPIO.output(B, True)
	GPIO.output(C, False)
	GPIO.output(D, True)
	time.sleep(delay)
	
def Step4():
	GPIO.output(A, False)
	GPIO.output(B, True)
	GPIO.output(C, True)
	GPIO.output(D, False)
	time.sleep(delay)
	
def move():
        sensorstate = GPIO.input(sensor)
        while sensorstate == 1:
                Step1()
                Step2()
                Step3()
                Step4()
            sensorstate = GPIO.input(sensor)
            
        sleep(2)
            
        if sensorstate == 0:
            
            schrittzahl = 22
            
            for i in range(schrittzahl):
                Step1()
                Step2()
                Step3()
                Step4()
                
        GPIO.output(A, False)
        GPIO.output(B, False)
        GPIO.output(C, False)
        GPIO.output(D, False)        

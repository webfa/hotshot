# -*- coding: cp1252 -*-

import xml.etree.ElementTree as ET
from sys import exc_info

class Settings(object):
    def __init__(self,path):
        #Parse XML tree for settings
        try:
            self.path = path
            self.tree = ET.parse(self.path)
            self.root = self.tree.getroot()

        #deal with input/output errors
        except IOError:
            print "IOError: Can't open settings file"

        #deal with unknown errors
        except:
            print "Unkown Error: " + str(exc_info()[0])

    def __del__(self):
        pass

    def save(self):
        #save XML tree to original path
        try:
            self.tree.write(self.path)

        except IOError:
            print "IOError: Can't write to settings file"

        except:
            print "Unknown Error: " + str(exc.info()[0])

    def getDrinks(self):
        drinks = []
        elements = self.root.findall(".drinks/drink")

        if elements:
            for element in elements:
                drink = {"name":str(element.get("name")),"pin":element.get("pin")}
                drinks.append(drink)

            return drinks

        else:
            print "Parent element not found"
            return None

    def setDrink(self, name, pin):
        element = self.root.find("./drinks/drink/[@pin='"+str(pin)+"']")

        if element is not None:
            element.set("name",name)
            self.save()
            return True

        else:
            print "Not possible to change fixing"
            return False

    def getUsers(self):
        users = []
        elements = self.root.findall("./users/user")

        if elements:
            for element in elements:
                user = {"name":str(element.get("name")),"pwd":str(element.get("pwd"))}
                users.append(user)

            return users

        else:
            print "No users found"
            return None

    def getFlowConstant(self):
        flowConstant = self.root.find("./maschine/flowrate")

        if flowConstant is not None:
            return float(flowConstant.text)
        
        else:
            return None

    def getSlots(self):
        slots = self.root.find("./maschine/slots")

        if slots is not None:
            return int(slots.text)

        else:
            return None

    def getSteps(self):
        steps = self.root.find("./maschine/steps")

        if steps is not None:
            return int(steps.text)

        else:
            return None

    def getQuantity(self):
        quantity = self.root.find("./drinks/quantity")

        if quantity is not None:
            return float(quantity.text)

        else:
            return None

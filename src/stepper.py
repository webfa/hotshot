import GPIO_Fake as GPIO
import time

class Stepper(object):
    def __init__(self,number_of_steps, motor_pin_1, motor_pin_2, motor_pin_3, motor_pin_4):
        self.step_number = 0
        self.direction = 0
        self.last_step_time = 0
        
        self.number_of_steps = number_of_steps

        self.step_delay = 0

        self.motor_pin_1 = motor_pin_1
        GPIO.setup(motor_pin_1,GPIO.OUT)
        self.motor_pin_2 = motor_pin_2
        GPIO.setup(motor_pin_2,GPIO.OUT)
        self.motor_pin_3 = motor_pin_3
        GPIO.setup(motor_pin_3,GPIO.OUT)
        self.motor_pin_4 = motor_pin_4
        GPIO.setup(motor_pin_4,GPIO.OUT)

    def setSpeed(self,whatSpeed):
        self.step_delay = 60.0/(self.number_of_steps*float(whatSpeed))
        print self.step_delay

    def step(self,steps_to_move):
        steps_left = abs(steps_to_move)

        if steps_to_move > 0:
            self.direction = 1

        if steps_to_move < 1:
            self.direction = -1

        while steps_left > 0:
            now = time.time()
            if(now - self.last_step_time >= self.step_delay):
                self.last_step_time = now

                if self.direction == 1:
                    self.step_number += 1;
                    if self.step_number == self.number_of_steps:
                        self.step_number = 0

                else:
                    if self.step_number == 0:
                        self.step_number = self.number_of_steps

                    self.step_number -= 1
                steps_left -=1

                self.stepMotor(self.step_number % 4)

    def stepMotor(self,thisStep):
        if thisStep == 0:
            GPIO.output(self.motor_pin_1,GPIO.HIGH)
            GPIO.output(self.motor_pin_2,GPIO.LOW)
            GPIO.output(self.motor_pin_3,GPIO.HIGH)
            GPIO.output(self.motor_pin_4,GPIO.LOW)
            
        elif thisStep == 1:
            GPIO.output(self.motor_pin_1,GPIO.LOW)
            GPIO.output(self.motor_pin_2,GPIO.HIGH)
            GPIO.output(self.motor_pin_3,GPIO.HIGH)
            GPIO.output(self.motor_pin_4,GPIO.LOW)

        elif thisStep == 2:
            GPIO.output(self.motor_pin_1,GPIO.LOW)
            GPIO.output(self.motor_pin_2,GPIO.HIGH)
            GPIO.output(self.motor_pin_3,GPIO.LOW)
            GPIO.output(self.motor_pin_4,GPIO.HIGH)

        elif thisStep == 3:
            GPIO.output(self.motor_pin_1,GPIO.HIGH)
            GPIO.output(self.motor_pin_2,GPIO.LOW)
            GPIO.output(self.motor_pin_3,GPIO.LOW)
            GPIO.output(self.motor_pin_4,GPIO.HIGH)
        


if __name__=="__main__":
    stepper = Stepper(200,1,2,3,4)
    stepper.setSpeed(60)
    stepper.step(200)

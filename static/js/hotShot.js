function hotShot(maxDrinks, variants)
{
	this.max = maxDrinks;
	this.min = 0;
	this.maxNumberOfCompetitors = maxDrinks;
	this.variants = variants;
	this.numberPerDrink = [];
	this.numberOfCompetitors = 0;
	
	console.log('Constructor');
	
	for(i=0; i < variants; i++){
		this.numberPerDrink.push(0);
	}
	
	this.checkForMaxNumberOfDrinks = function()
	{
		var sum = 0;
		for(i=0; i < this.numberPerDrink.length; i++)
		{
			sum = sum + this.numberPerDrink[i];
		}
		
		if(sum >= this.min && sum < this.max)
		{
			return true;
		}
		else
		{
			$('#myAlert').show("fast");
			console.log('Zu Viel');
			return false;
		}
	}
	
	this.updateView = function(drinkNumber, identifier)
	{
		$(identifier).text(this.numberPerDrink[drinkNumber]);
		console.log(this.numberPerDrink);
	}
	
	this.addToDrink = function(drinkNumber, identifier)
	{
		if(this.checkForMaxNumberOfDrinks()){
			this.numberPerDrink[drinkNumber] = this.numberPerDrink[drinkNumber] + 1;
		}
		this.updateView(drinkNumber, identifier);
	}
	
	this.subtractToDrink = function(drinkNumber, identifier)
	{
		if(this.numberPerDrink[drinkNumber] > 0){
			$('#myAlert').hide();
			this.numberPerDrink[drinkNumber] = this.numberPerDrink[drinkNumber] - 1;
		}
		this.updateView(drinkNumber, identifier);
	}
	
	this.resetNumberOfDrinks = function()
	{
		console.log('Reset');
		for(i=0; i < this.numberPerDrink.length; i++){
			this.numberPerDrink[i] = 0;
			$('.statusLabel').text('0');
		}
	}
	
	this.resetNumberOfCompetitors = function()
	{
		this.numberOfCompetitors = 0;
		console.log(this.numberOfCompetitors);
		$('#competitorsLabel').text('0');
	}
	
	this.addCompetitor = function()
	{
		if(this.numberOfCompetitors < this.maxNumberOfCompetitors)
		{
			this.numberOfCompetitors = this.numberOfCompetitors +1;
			$('#competitorsLabel').text(this.numberOfCompetitors);
		}
		else
		{
			$('#myAlert2').show("fast");
		}
	}
	
	this.subtractCompetitor = function()
	{
		if(this.numberOfCompetitors > 0)
		{
			this.numberOfCompetitors = this.numberOfCompetitors -1;
			$('#competitorsLabel').text(this.numberOfCompetitors);
			$('#myAlert2').hide();
		}
	}
}
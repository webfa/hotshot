# HotShot
HotShot is a Software to serve shorts. It is based on the Flask project and runs on the Raspberry Pi.

## Prerequisites
To run the code you need:
+ [Flask](http://flask.pocoo.org/)
+ [LIGHTHTTPD](https://www.lighttpd.net/)

## Download
To clone the code use

``git clone https://webfa@bitbucket.org/webfa/hotshot.git``.


To update the code base use


``git pull``


command.